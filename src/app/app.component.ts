import { Component } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import {saveAs as importedSaveAs} from "file-saver";

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.css']
})
export class AppComponent {

    texto: string = "texto por defecto";
    imageSrc: any;
    imageBuild: any;
    file: any;
    estado: boolean = false;

    constructor(private http: HttpClient) { }

    fileChangeEvent(fileInput: any): void {
        if (fileInput.target.files && fileInput.target.files[0]) {
            const reader = new FileReader();
            this.file = fileInput.target.files[0];
            reader.onload = ((e) => {
                //this.imageSrc = e.target['result'];
            });

            reader.readAsDataURL(fileInput.target.files[0]);
        }
    }

    private crearImagen(): void {

        const httpOptions = {
            headers: new HttpHeaders({
                'Content-Type': 'multipart/form-data'
            })
        };
        const headers = new HttpHeaders().set('Content-Type', 'multipart/form-data');
        const formData = new FormData();

        formData.append("file", this.file);
        this.http.post<any>("http://127.0.0.1:8080/v1/imagen", formData)
            //.pipe(map((res: Response) => res.blob()))
            .subscribe(resp => {
                this.estado = true;
                this.solicitarImagen();

            }, console.error);
    }

    solicitarImagen(): void {
        this.http.get('http://127.0.0.1:8080/v1/imagen',{responseType: "blob"})
            .subscribe((blob) => {
                this.descargarArchivo(blob);
            });
    }

    // para una imagen.
    /*createImageFromBlob(blob: Blob) {
        let reader = new FileReader();
        reader.addEventListener("load", () => {
           this.imageSrc = reader.result;
        }, false);
     
        if (blob) {
           reader.readAsDataURL(blob);
        }
     }*/

     descargarArchivo(blob:Blob):void{
        importedSaveAs(blob, "imagenes-generadas.zip");
     }
}

